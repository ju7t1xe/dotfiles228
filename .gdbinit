set $64BITS = 1
set disassembly-flavor intel
set pagination off
set confirm off

focus cmd
winheight cmd 20

define hook-stop
info args
info locals
end
