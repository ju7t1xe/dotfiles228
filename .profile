append_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

append_path "$HOME/.local/bin"
append_path "$HOME/.cargo/bin"
export PATH

export LC_ALL=en_US.UTF-8 
export EDITOR=v

export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config

export NNN_TRASH=1
export NNN_FIFO=/tmp/nnn.fifo

# export DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1=1
# export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/radeon_icd.i686.json:/usr/share/vulkan/icd.d/radeon_icd.x86_64.json
