setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense
setopt noclobber           # prevents you from accidentally overwriting an existing file

HISTFILE=~/.cache/histfile
HISTSIZE=5000
SAVEHIST=5000
WORDCHARS=''
unsetopt beep
unsetopt pathdirs
bindkey -e

setopt PROMPT_CR
setopt PROMPT_SP
export PROMPT_EOL_MARK=""

zstyle :compinstall filename '/home/korei/.zshrc'
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' completer _oldlist _complete _ignored _match _approximate
zstyle ':completion:*' menu select

autoload -Uz compinit
for dump in ~/.zcompdump(N.mh+24); do
    compinit
done
compinit -C

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
bindkey '^ ' autosuggest-accept
export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
set ZSH_AUTOSUGGEST_MANUAL_REBIND

autoload -U add-zsh-hook
osc7()
{
    local LC_ALL=C
    export LC_ALL

    setopt localoptions extendedglob
    input=( ${(s::)PWD} )
    uri=${(j::)input/(#b)([^A-Za-z0-9_.\!~*\'\(\)-\/])/%${(l:2::0:)$(([##16]#match))}}
    print -n "\e]7;file://${HOSTNAME}${uri}\e\\"
}
add-zsh-hook -Uz chpwd osc7

n()
{
    if [[ "${NNNLVL:-0}" -ge 1 ]]; then
        echo "nnn is already running"
        return
    fi

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    \nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias ip='ip --color=auto'

alias xgif="byzanz-record --height=1080 --width=1920"
alias wgif="wf-recorder -g \"\$(slurp)\" -F fps=14 -c gif -f /tmp/rec.gif"
alias wrec="wf-recorder --audio -c hevc_vaapi -f /tmp/rec_test.mp4"
alias wprop="sleep 2; sway-prop"
alias hprop="sleep 2; hyprctl activewindow"
alias dprop="sleep 2; dwlmsg -g -c"
alias rm="trash-put"
alias lsf="find . -maxdepth 1 -type f"
alias kt="pkill -f tmux"
alias ftn="foot --log-level none & disown"
alias stn="st & disown"
alias btt="bluetooth toggle"
alias amd="sudo watch -n 0.5 cat /sys/kernel/debug/dri/0/amdgpu_pm_info"
alias igpu="watch -n 0.5 cat /sys/class/drm/card0/gt_cur_freq_mhz"
alias mpvm="mpv --vo=null --video=no --no-video --term-osd-bar --no-resume-playback"
alias hp="echo \$(hyprpicker)"
alias cy="cal -y"
alias ll="ls -lh"

vidtogif()
{
    ffmpeg -ss $1 -t $2 -i $3 \
        -vf "fps=15,scale=640:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" \
        -loop 0 $4
}

killp()
{
    pkill $(ps auxc | sk | awk '{ print $11 }')
}

get_gpu_mode()
{
    cat /sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0/0000:02:00.0/0000:03:00.0/pp_power_profile_mode
}

mousebattery()
{
    upower --dump | rg -A 12 -i "gaming|mouse"
}

vol()
{
    wpctl set-volume @DEFAULT_AUDIO_SINK@ $1%
    pkill -RTMIN+12 someblocks
}

bindkey '^[[Z' reverse-menu-complete
bindkey "^[[1~"   beginning-of-line
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey "^[[4~"   end-of-line
bindkey  "^[[3~"  delete-char
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word
bindkey '^[^?' backward-kill-word

export NNN_OPTS="HdA"
export NNN_PLUG='p:preview-tui;d:dragdrop;i:imgview;u:mypreview'
export NNN_BMS="c:$HOME/.config/;h:$HOME/;l:$HOME/.local/;t:$HOME/.local/share/Trash/files/;T:/tmp/;p:$HOME/Pictures/;s:$HOME/.steam/root/compatibilitytools.d/;v:$HOME/Videos/;m:/run/media/$USER;b:$HOME/Documents/books;d:$HOME/Downloads/;i:$HOME/.cache/vis-bak/;w:$HOME/source/wl/;W:$HOME/source/wl/NEXT/dwl/;g:$HOME/Documents/GL/opengl/Triangle/;D:$HOME/source/dotfiles/;a:$HOME/Documents/algo/Skiena/"

autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '*%b'

setopt PROMPT_SUBST
NL=$'\n'
PROMPT='${NL}%F{white}%n@%m %F{cyan}%~%f %F{#f0c674}${vcs_info_msg_0_}%f${NL}%%_ '
