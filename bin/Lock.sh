#!/bin/sh

LOCK_BG="/home/korei/Pictures/wallpapers/ds_drg.png"
LOCK="swaylock -k -f -c 121212 -i $LOCK_BG -s fill --font Consolas --font-size 24 --indicator-radius 90 --indicator-thickness 15"

# DPMS_OFF='for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --off; done'
# DPMS_ON='for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --on; done'
DPMS_OFF='for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlopm --off "${o}"; done'
DPMS_ON='for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlopm --on "${o}"; done'

def()
{
    swayidle -w \
        timeout 300 "$DPMS_OFF" \
        resume "$DPMS_ON" \
        timeout 600 "systemctl suspend" \
        before-sleep "$LOCK"
        # after-resume
}

tog()
{
    if [ "$(pgrep -c swayidle)" -ge 1 ]
    then
        killall swayidle & notify-send -u low "swayidle" "off"
    else
        def & notify-send -u low "swayidle" "on"
    fi
}

on()
{
    if ! [ "$(pgrep -c swayidle)" -ge 1 ]
    then
        def & echo "swayidle is on"
    else
        echo "swayidle is on"
    fi
}

off()
{
    killall swayidle & echo "swayidle is off"
}

show()
{
    if ! [ "$(pgrep -c swayidle)" -ge 1 ]
    then
        echo "idle: off"
    else
        echo "idle: on"
    fi
    pkill -RTMIN+13 someblocks
}

case "$1" in
    lock) playerctl pause & $LOCK ;;
    toggle) tog ;;
    on) on ;;
    off) off ;;
    show) show ;;
    *) def ;;
esac
