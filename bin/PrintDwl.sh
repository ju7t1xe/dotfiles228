#!/bin/sh

set -e

MONS="HDMI\|DP\|LVDS\|VGA"

Save()
{
    str=$(wlr-randr | grep $MONS | awk '{print $1}')

    if [ $(echo "$str" | wc -l) -ge 2 ]
    then
        grim -o $(echo "$str" | fuzzel -d) $HOME/Pictures/Screens/$(date +'%Y-%m-%d-%H_%M_%S-screenshot.webp')
    else
        grim -o "$str" $HOME/Pictures/Screens/$(date +'%Y-%m-%d-%H_%M_%S-screenshot.webp')
    fi

    grim -o $(wlr-randr | grep $MONS | awk '{print $1}') $HOME/Pictures/Screens/$(date +'%Y-%m-%d-%H_%M_%S-screenshot.webp')
    notify-send "Saved" "to ~/Pictures/Screens/" -t 2000
}

SelectSave()
{
    slurp | grim -g - $HOME/Pictures/Screens/$(date +'%Y-%m-%d-%H_%M_%S-screenshot.webp')
    notify-send "Saved" "to ~/Pictures/Screens/" -t 2000
}

Select()
{
    grim -g "$(slurp -d)" - | wl-copy
    notify-send "Saved" "to \nclipboard" -t 2000
}

def()
{
    str=$(wlr-randr | grep $MONS | awk '{print $1}')

    if [ $(echo "$str" | wc -l) -ge 2 ]
    then
        grim -o $(echo "$str" | fuzzel -d) - | wl-copy
    else
        grim -o "$str" - | wl-copy
    fi

    notify-send "Saved" "to \nclipboard" -t 2000
}

case "$1" in
    Save) Save ;;
    Select) Select ;;
    SelectSave) SelectSave ;;
    *) def ;;
esac
