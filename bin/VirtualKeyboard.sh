#!/bin/sh

if [ "$(pgrep -c wvkbd-mobintl)" -ge 1 ]
then
    killall wvkbd-mobintl
else
    wvkbd-mobintl
fi

