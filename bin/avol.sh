#!/bin/zsh

sts=$(amixer -D pulse get Master)
sformat=$(echo "$sts" | awk -F 'Right:|[][]' 'BEGIN {RS=""}{ print $4 }')


if [[ "$sformat" == "on" ]] then
    vol=$(echo "$sts" | awk -F 'Right:|[][]' 'BEGIN {RS=""}{ print $2 }')
    echo "vol: "$vol""
else
    echo "vol: Muted"
fi
