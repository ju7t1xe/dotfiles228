#!/bin/zsh

cap="$(cat /sys/class/power_supply/BAT0/capacity)"

if [[ "$(cat /sys/class/power_supply/BAT0/status)" == "Charging" ]] then
    echo "*"$cap"%"
else
    echo ""$cap"%"
fi
