#!/bin/sh

entries="Lock\nLogout\nSuspend\nReboot\nShutdown"

# launcher="dmenu-wl -i"
launcher="fuzzel -d"

selected=$(echo $entries| $launcher | awk '{print tolower($1)}')

case $selected in
    lock)
        Lock.sh on
        Lock.sh lock
        ;;
    logout)
        swaymsg exit & killall dwl & killall dwm & killall Hyprland
        ;;
    suspend)
        Lock.sh on
        playerctl -a pause
        systemctl suspend
        ;;
    reboot)
        systemctl reboot
        ;;
    shutdown)
        systemctl poweroff
        ;;
esac
