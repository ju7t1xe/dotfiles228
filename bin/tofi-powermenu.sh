#!/bin/sh

entries="Lock\nLogout\nSuspend\nReboot\nShutdown"

menu="fuzzel -d"

selected=$(echo $entries| $menu | awk '{print tolower($1)}')

case $selected in
	lock)
		Lock.sh on
		Lock.sh lock
		;;
	logout)
		swaymsg exit & killall dwl & killall dwm & killall Hyprland
		;;
	suspend)
		Lock.sh on
		playerctl -a pause
		systemctl suspend
		;;
	reboot)
		systemctl reboot
		;;
	shutdown)
		systemctl poweroff
		;;
esac
