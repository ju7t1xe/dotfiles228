#!/bin/sh
set -e

l()
{
    wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 3%-
    pkill -RTMIN+12 someblocks
}

r()
{
    wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 3%+
    pkill -RTMIN+12 someblocks
}

m()
{
    wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
    pkill -RTMIN+12 someblocks
}

case "$1" in
    l) l ;;
    r) r ;;
    m) m ;;
    *) ;;
esac
