#!/bin/sh

entries="Lock\nLogout\nSuspend\nReboot\nShutdown"

if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
  launcher='fuzzel -d'
else
  launcher='dmenu'
fi

selected=$(echo $entries| $launcher | awk '{print tolower($1)}')

case $selected in
	lock)
		Lock.sh on
		Lock.sh lock
		;;
	logout)
		swaymsg exit & killall dwl & killall dwm & killall Hyprland
		;;
	suspend)
		Lock.sh on
		playerctl -a pause
		systemctl suspend
		;;
	reboot)
		systemctl reboot
		;;
	shutdown)
		systemctl poweroff
		;;
esac
