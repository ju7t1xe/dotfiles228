#!/bin/sh

killall -q polkit-gnome-authentication-agent-1
killall -q playerctld
killall -q someblocks
killall -q swayidle
killall -q mpris-proxy
killall -q wlsunset
killall -q foot

if [ $(wlr-randr | grep -ci "DP-2") -ge 1 ]; then
    # wlr-randr --output HDMI-A-1 --mode 1400x1050@84.959999Hz
    # wlr-randr --output DP-2 --mode 1400x1050@59.978001Hz
    wlr-randr --output DP-2 --mode 1600x1200@60
    # wlr-randr --output DP-2 --mode 1280x1024@60.020000Hz
    # wlr-randr --output HDMI-A-1 --mode 1600x1200@60.000000Hz
    # wlr-randr --output DP-1 --off
    dwlmsg -s -l 2
fi
# wlr-randr --output DP-2 --mode 1280x960 &
# wlr-randr --output HDMI-A-1 --mode 1920x1080@239.759995 &
# wlr-randr --output DP-2 --mode 1920x1080@239.759995 &

# $HOME/.dwm/waybar-dwl.sh &
# waybar -c $HOME/.config/dwl/waybar/config -s $HOME/.config/dwl/waybar/style.css &

systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

dbus-update-activation-environment --systemd --all
systemctl --user import-environment QT_QPA_PLATFORMTHEME

gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.interface gtk-theme 'Graphite-blue-Dark'
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita'
gsettings set org.gnome.desktop.interface font-name 'DejaVu Sans 10'

foot --server &

dwlb -font "DejaVu Sans:size=10.5:weight=bold" -ipc &
someblocks -p | dwlb -status-stdin all &
# yambar &
# somebar &
# someblocks &
# dwl-bar &
# someblocks -s /run/user/1000/dwl-bar-0 &
# someblocks -p | while read -r line; do dwlb -status all "$line"; done &

playerctld daemon &
mpris-proxy &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
mako &
# wbg /home/korei/Pictures/wallpapers/stone2.jpg &
# swaybg -i "/home/korei/Pictures/wallpapers/ds_up1.png" -m fill &
swaybg -i "/home/korei/Pictures/wallpapers/yosemite-stargazing.jpg" -m fill &
# wlsunset -S 10:30 -s 23:59 -T 6500 -t 5000 &

# wlr-randr --output DP-1 --mode 1920x1080@239.759995Hz &
# wlr-randr --output DP-1 --mode 1920x1080@119.982002Hz &
# wlr-randr --output DP-1 --mode 1920x1080@60.000000Hz &

portals.sh &
Lock.sh &

# corectrl & sleep 2; killall corectrl
