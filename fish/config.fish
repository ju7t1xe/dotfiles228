if status is-interactive
    # Commands to run in interactive sessions can go here

    set -g fish_term24bit 1
    bind -k nul accept-autosuggestion
    set -g fish_greeting

    # if [ $TERM != "foot" ]
    # fish_add_path -aP /home/korei/.local/bin /home/korei/.cargo/bin

    # set -gx LC_ALL en_US.UTF-8 
    # set -gx EDITOR v

    # set -gx NNN_TRASH 1
    # set -gx NNN_FIFO /tmp/nnn.fifo

    # export XDG_DATA_HOME=$HOME/.local/share
    # export XDG_CONFIG_HOME=$HOME/.config
    # end

    export NNN_OPTS="HdA"
    export NNN_PLUG='p:preview-tui;d:dragdrop;i:imgview;u:mypreview'
    export NNN_BMS="c:$HOME/.config/;h:$HOME/;l:$HOME/.local/;t:$HOME/.local/share/Trash/files/;T:/tmp/;p:$HOME/Pictures/;s:$HOME/.steam/root/compatibilitytools.d/;v:$HOME/Videos/;m:/run/media/$USER;b:$HOME/Documents/books;d:$HOME/Downloads/;i:$HOME/.cache/vis-bak/;w:$HOME/source/wl/;W:$HOME/source/wl/NEXT/dwl/;g:$HOME/Documents/GL/opengl/Triangle/;D:$HOME/source/dotfiles/;a:$HOME/Documents/algo/Skiena/"

    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'
    alias ll='ls -lh'

    alias wgif="wf-recorder -g \"\$(slurp)\" -F fps=14 -c gif -f /tmp/rec.gif"
    alias wrec="wf-recorder --audio -c hevc_vaapi -f /tmp/rec_test.mp4"
    alias wprop="sleep 2; sway-prop"
    alias hprop="sleep 2; hyprctl activewindow"
    alias dprop="sleep 2; dwlmsg -g -c"
    alias rm="trash-put"
    alias lsf="find . -maxdepth 1 -type f"
    alias kt="pkill -f tmux"
    alias ftn="foot --log-level none & disown"
    alias stn="st & disown"
    alias btt="bluetooth toggle"
    alias amd="sudo watch -n 0.5 cat /sys/kernel/debug/dri/0/amdgpu_pm_info"
    alias igpu="watch -n 0.5 cat /sys/class/drm/card0/gt_cur_freq_mhz"
    alias mpvm="mpv --vo=null --video=no --no-video --term-osd-bar --no-resume-playback"
    alias hp="echo \$(hyprpicker)"
    alias cy="cal -y"
end
