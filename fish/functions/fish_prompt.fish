function fish_prompt
    printf "\n$USER@$hostname " 
    set_color cyan
    printf $PWD
    set_color yellow
    printf '%s' (fish_vcs_prompt)\*
    set_color -o white
    echo \n"> "(set_color normal)
end
