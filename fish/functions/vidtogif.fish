function fish_right_prompt
    ffmpeg -ss $1 -t $2 -i $3 \
        -vf "fps=15,scale=640:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" \
        -loop 0 $4
end
