export WLR_RENDERER=gles2

export XDG_CURRENT_DESKTOP=Hyprland
export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=Hyprland

export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_QPA_PLATFORM="wayland;xcb"
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export QT_QPA_PLATFORMTHEME=qt5ct

export MOZ_ENABLE_WAYLAND=1
export MOZ_WEBRENDER=1

export _JAVA_AWT_WM_NONREPARENTING=1
export CLUTTER_BACKEND="wayland"
export SDL_VIDEODRIVER="wayland,x11"

export GTK_THEME="Graphite-blue-Dark"
export XCURSOR_THEME="Adwaita"
export XCURSOR_SIZE="28"

export GDK_BACKEND="wayland,x11"
export PIPEWIRE_LATENCY="128/48000"
export LAUNCHER=tofi

Hyprland
