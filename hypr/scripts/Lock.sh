#!/bin/sh

LOCK_BG="/home/korei/Pictures/wallpapers/ds_drg.png"
LOCK="swaylock -k -f -c 121212 -i $LOCK_BG -s fill --font Consolas --font-size 24 --indicator-radius 90 --indicator-thickness 15"

def() {
	swayidle -w \
		timeout 300 "hyprctl dispatch dpms off" \
		resume "hyprctl dispatch dpms on" \
		timeout 600 "systemctl suspend" \
		before-sleep "$LOCK"
}

tog()
{
    if [ "$(pgrep -c swayidle)" -ge 1 ]
    then
        killall swayidle & notify-send -u low "swayidle" "off"
    else
        def & notify-send -u low "swayidle" "on"
    fi
}

on()
{
    if ! [ "$(pgrep -c swayidle)" -ge 1 ]
    then
        def & echo "swayidle is on"
    else
        echo "swayidle is on"
    fi
}

off()
{
    killall swayidle & echo "swayidle is off"
}

case "$1" in
    lock) playerctl pause & $LOCK ;;
    toggle) tog ;;
    on) on ;;
    off) off ;;
    *) def ;;
esac
