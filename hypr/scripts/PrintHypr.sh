#!/bin/sh

# \rm -r /tmp/wl-copy-buffer*

grim -o $(hyprctl -j monitors | jq -r '.[] | select(.focused) | .name') - | wl-copy
notify-send "Saved" "to \nclipboard" -t 2000
