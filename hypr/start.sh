#!/bin/sh

killall -q polkit-gnome-authentication-agent-1
killall -q playerctld
killall -q someblocks
killall -q mako
killall -q waybar
killall -q foot

systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
playerctld daemon &
mpris-proxy &
swaybg -i "/home/korei/Pictures/wallpapers/yosemite-stargazing.jpg" -m fill &
waybar -c $HOME/.config/hypr/waybar/config -s $HOME/.config/hypr/waybar/style.css &
mako &
foot --server &

# wlsunset -S 10:00 -s 23:59 -T 6500 -t 5000 &

# $HOME/.config/hypr/portals.sh &

$HOME/.config/hypr/scripts/Lock.sh &
# portals.sh &

# udisksctl mount -b /dev/$(lsblk -l -o name,mountpoint,label,size | grep -i "evo" | awk '{ print $1 }')

# Lock.sh &

# corectrl & sleep 2; killall corectrl

for i in $(seq 10 -1 1)
do
    hyprctl dispatch workspace $i
done
