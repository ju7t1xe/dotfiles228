#!/bin/zsh

LOCK_BG="/home/korei/Pictures/wallpapers/carpathians.jpg"
LOCK="swaylock -f -c 121212 -i $LOCK_BG -s fill --font Spleen --font-size 24 --indicator-radius 90 --indicator-thickness 15"

function def
{
  swayidle -w \
    timeout 180 "playerctl -a pause" \
    timeout 180 "hyprctl dispatch dpms off" \
    resume "hyprctl dispatch dpms on" \
    timeout 500 "systemctl suspend" \
    before-sleep "$LOCK"
  }

function tog
{
  if [[ "$(pgrep swayidle)" =~ ^[0-9]+$ ]]
  then
    killall swayidle & notify-send -u low "swayidle" "off"
  else
    def & notify-send -u low "swayidle" "on"
  fi
}

case "$1" in
  lock) playerctl pause & sh -c "$LOCK"
    ;;
  toggle) tog
    ;;
  *) def
    ;;
esac
