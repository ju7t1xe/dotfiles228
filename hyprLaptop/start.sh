#!/bin/sh

killall -q polkit-gnome-authentication-agent-1
killall -q playerctld
killall -q someblocks
killall -q mako
killall -q nm-applet
killall -q blueman-applet
killall -q waybar

systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP &
dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
playerctld daemon &
nm-applet &
# blueman-applet &
# wbg /home/korei/Pictures/Screens/parish.png &
swaybg -i "/home/korei/Pictures/wallpapers/hubky_castle.jpg" -m fill &
waybar -c $HOME/.config/hypr/waybar/config -s $HOME/.config/hypr/waybar/style.css &
mako &
foot --server &
# portals.sh &
# $HOME/.config/hypr/portals.sh &
$HOME/.config/hypr/scripts/Lock.sh &

bluetooth off
wlsunset -T 6500 -t 5000 -S 11:00 -s 23:59 &

# arrange 2 monitors
# if [[ $(wlr-randr | grep -c DP-1) == 1 ]]
# then
	# wlr-randr --output LVDS-1 --pos 1920,0
	# wlr-randr --output DP-1 --pos 0,0
	# # wlr-randr --output DP-1 --mode 1920x1080@60.000000Hz
	# wlr-randr --output LVDS-1 --off
# fi

# corectrl & sleep 2; killall corectrl
