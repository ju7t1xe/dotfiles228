vim.opt.termguicolors = true

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")

require("p_config.toggleTerm")
require("p_config.colorizer")
require("p_config.lualine")
require("p_config.telescope")
require("p_config.lsp")
-- require("p_config.mason")
require("p_config.cmp")
-- require("p_config.treesitter")
-- require("p_config.line")

require("general")
require("comments")
require("maps")
