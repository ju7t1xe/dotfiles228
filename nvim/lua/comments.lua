local map = vim.keymap.set

map("n", "gc", "I# <Esc>^")
map("n", "gC", ":.s/# //<CR>^")
map("v", "gc", [[:s/\%V\([!-~].*\)\%V/# \1/<CR>]])
map("v", "gC", ":s/# //<CR>^")

vim.cmd('autocmd FileType * lua setKeybinds()') function setKeybinds()
    local fileType = vim.api.nvim_buf_get_option(0, "filetype")

    if fileType == "c" or fileType == "cpp" or fileType == "glsl" then
        map("n", "gb", "I/* <Esc>$a */<Esc>^")
        map("n", "gB", ":.s/\\/\\* \\| \\*\\///g<CR>^")
        map("v", "gb", [[:s/\%V\([!-~].*\)\%V/\/* \1 *\//<CR>]])
        map("v", "gB", ":s/\\/\\* \\| \\*\\///g<CR>^")

        map("n", "gc", "I// <Esc>^")
        map("n", "gC", ":.s/\\/\\/ //g<CR>^")
        map("v", "gc", [[:s/\%V\([!-~].*\)\%V/\/\/ \1/<CR>]])
        map("v", "gC", ":s/\\/\\/ //g<CR>^")
    elseif fileType == "lua" then
        map("n", "gc", "I-- <Esc>^")
        map("n", "gC", ":.s/-- //<CR>^")
        map("v", "gc", [[:s/\%V\([!-~].*\)\%V/-- \1/<CR>]])
        map("v", "gC", ":s/-- //<CR>^")
    end
end
