-- require("tomorrow-custom").setup({
    -- transparent = false,
    -- styles = {
        -- comments = {},
        -- keywords = { fg = "#8abeb7", bold = false, italic = true },
        -- functions = {},
        -- variables = {},
        -- type = { bold = true },
        -- structures = { bold = true, italic = true },
        -- lsp = {},
    -- },
    -- custom_colors = {
        -- none = "NONE",
        -- fg = "#cccccc",
        -- bg = "#121212",
        -- alt_bg = "#1a1a1a",
        -- accent = "#202020",
        -- white = "#cccccc",
        -- gray = "#373737",
        -- medium_gray = "#727272",
        -- light_gray = "#bababa",
        -- blue = "#81a2be",
        -- cyan = "#8abeb7",
        -- red = "#cc6666",
        -- yellow = "#f0c674",
        -- purple = "#b294bb",
        -- cursor_fg = "#0f0f0f",
        -- cursor_bg = "#cccccc",
    -- },
-- })

vim.cmd [[colorscheme tomorrow-custom]]
-- -- remove lsp bullshit
-- for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
    -- vim.api.nvim_set_hl(0, group, {})
-- end

vim.cmd [[let b:match_words = '<:>']] -- fix c++
vim.o.guifont = "Consolas:h15"
vim.opt.exrc = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.hlsearch = false
vim.opt.hidden = true
vim.opt.clipboard = "unnamedplus"
vim.opt.mouse = "a"
vim.opt.swapfile = false
vim.opt.smartcase = true
vim.opt.guicursor = "i:block"
-- vim.opt.guicursor = "n:hor20-Cursor/lCursor"
vim.opt.cmdheight = 1
vim.opt.scrolloff = 8

-- Persistent undo
vim.opt.undofile = true
vim.opt.undodir = os.getenv("HOME") .. '/.cache/nvim/undo'
vim.opt.undolevels = 1000
vim.opt.undoreload = 10000

-- Stop comments on newline
vim.api.nvim_create_autocmd("BufEnter", {command = "set formatoptions-=c formatoptions-=r formatoptions-=o"})

vim.api.nvim_create_autocmd("BufEnter", {command = "set nowrap"})

-- set syntax for filetypes
vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*.frag", "*.vert", "*.fs", "*.vs", "*.gs", "*.glsl", "*.geom"},
    command = "set filetype=glsl"
})
-- vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
    -- pattern = {"*.sh", ".zshrc", ".zprofile", ".zlogin", ".zshenv", ".yashrc", ".yash_profile"},
    -- command = "set filetype=bash"
-- })
-- vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
-- pattern = {"*.h"},
-- command = "set filetype=c"
-- })
vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*rc"},
    command = "set filetype=config"
})

vim.api.nvim_create_autocmd({"TermOpen"}, {
    command = "setlocal nonumber norelativenumber"
})
-- vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
-- pattern = {"*.asm", "*.s"},
-- command = "setlocal shiftwidth=8 \
-- setlocal tabstop=8",
-- })
