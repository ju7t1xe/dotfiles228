local map = vim.keymap.set
-- Move across buffers
map("n", "<A-j>", "<Cmd>bn<CR>")
map("n", "<A-k>", "<Cmd>bp<CR>")
map("n", "<A-c>", "<Cmd>bdelete<CR>")
map("t", "<A-j>", "<Cmd>bn<CR>")
map("t", "<A-k>", "<Cmd>bp<CR>")
map("t", "<A-c>", "<Cmd>bdelete<CR>")
-- tabs
map("n", "<A-p>", "<Cmd>tabn<CR>")
map("n", "<A-n>", "<Cmd>tabp<CR>")
map("n", "<A-e>", "<Cmd>tabnew<CR>")
map("t", "<A-p>", "<Cmd>tabn<CR>")
map("t", "<A-n>", "<Cmd>tabp<CR>")
map("t", "<A-e>", "<Cmd>tabnew<CR>")
-- move split line
map("n", "<C-l>", "2<C-w>>")
map("n", "<C-h>", "2<C-w><")
map("n", "<C-j>", "2<C-w>+")
map("n", "<C-k>", "2<C-w>-")
-- delete not cut
map("n", "d", "\"_d")
map("n", "D", "\"_D")
map("v", "d", "\"_d")
map("n", "x", "\"_x")
-- cut with X instead
map("n", "X", "dd")
-- c-d is delete
map("i", "<C-d>", "<Del>")
-- select text, escape, then replace word under cursor
map("n", "<space>v", [[:%s/\<\%V<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
map("n", "<space>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
-- enquote
map("n", "Q", "\"1ciw\"\"<Esc>\"1Pb")
map("n", "<C-q>", "\"1ciw\'\'<Esc>\"1Pb")
map("v", "Q", [[:s/\%V\(.*\)\%V/"\1"/<CR>]])
map("v", "<C-q>", [[:s/\%V\(.*\)\%V/'\1'/<CR>]])
map("n", "<space>\"", [[:%s/\<\%V<C-r><C-w>\>/"<C-r><C-w>"/gI<Left><Left><Left><CR>]])
map("n", "<space>\'", [[:%s/\<\%V<C-r><C-w>\>/'<C-r><C-w>'/gI<Left><Left><Left><CR>]])
-- paste to the end of line
map("n", "<space>a", "mzA<space><esc>p`z")
-- etc
map("t", "<Esc>", "<C-\\><C-n>")
map("n", "<M-q>", "<Cmd>term<CR>i")
map("t", "<M-q>", "<C-a><C-d>")
map("n", "<M-r>", "<Cmd>tabnew<CR><Cmd>term<CR>imake<CR>")
map("n", "J", "mzJ`z")
map('v', '<M-=>', [[:!clang-format --style="{BasedOnStyle: webkit, IndentWidth: 4, UseTab: Never, AlwaysBreakAfterReturnType: TopLevelDefinitions, AlignAfterOpenBracket: Align, AlignArrayOfStructures: Left, IndentCaseLabels: true, IndentPPDirectives: AfterHash, PackConstructorInitializers: NextLine, Cpp11BracedListStyle: true}"<CR><CR>]])
map('i', '<M-d>', '<Esc>e\"_diwi')
-- ban
map('n', 'q:', '<Nop>');
map('v', 'K', '<Nop>');
map('n', 'H', '<Nop>');
map('n', 'L', '<Nop>');

-- map('n', 'e', 'el')
