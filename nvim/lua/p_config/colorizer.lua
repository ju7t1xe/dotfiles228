require'colorizer'.setup {
    '*';
    cpp = { names = false }
}

vim.keymap.set('n', '<space>cc', ':ColorizerToggle<CR>')
