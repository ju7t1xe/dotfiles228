require('lualine').setup {
    options = {
        icons_enabled = false,
        theme = 'tomorrow-custom',
        component_separators = { left = '|', right = ''},
        section_separators = { left = '', right = ''},
        disabled_filetypes = {
            statusline = {},
            winbar = {},
        },
        ignore_focus = {},
        always_divide_middle = true,
        globalstatus = false,
        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        }
    },
    sections = {
        lualine_a = {'encoding', 'fileformat', 'filetype'},
        lualine_b = {'branch'},
        lualine_c = {'diagnostics'},
        lualine_x = {'buffers'},
        lualine_y = {'progress'},
        lualine_z = {'location'},
    },
    tabline = {},
    winbar = {},
    inactive_winbar = {},
    extensions = {}
}
