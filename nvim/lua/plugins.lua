return {
    -- colorschemes
    "korei999/tomorrow-custom-nvim",
    "nvim-lualine/lualine.nvim",
    -- "nvim-treesitter/nvim-treesitter",

    -- lsp
    "neovim/nvim-lspconfig",
    -- "williamboman/mason.nvim",
    -- "williamboman/mason-lspconfig.nvim",

    -- completions
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",

    -- telescope
    {
        "nvim-telescope/telescope.nvim", tag = "0.1.5",
        -- or                              , branch = '0.1.x',
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope-fzy-native.nvim"
        }
    },

    -- etc
    "akinsho/nvim-toggleterm.lua",
    "norcalli/nvim-colorizer.lua",
    -- { "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = {} }
}
