vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	if win.file.name then
		local filetype = win.file.name:match('.*%.(.*)')
		if filetype == 'glsl' 
			or filetype == 'fs'
			or filetype == 'vs'
			or filetype == 'vert'
			or filetype == 'geom'
			or filetype == 'frag'
			then vis:command("set syntax glsl")
		end
	end
end)
