local plug = require('plugins/vis-plug')

-- require and optionally install plugins on init
plug.path('/home/korei/.config/vis')

-- configure plugins in an array of tables with git urls and options 
local plugins = {
	{ 'https://repo.or.cz/vis-goto-file.git' },
	{ 'https://github.com/roguh/vis-backup' },
	{ 'https://github.com/kupospelov/vis-ctags' },
	{ 'https://github.com/ingolemo/vis-smart-backspace' },
    { 'https://github.com/thimc/vis-colorizer' }
}

plug.init(plugins, true)
