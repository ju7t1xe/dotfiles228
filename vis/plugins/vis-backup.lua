local backup = require('plugins/vis-backup/backup')
backup.byte_limit = 500000
backup.set_directory(os.getenv('HOME') .. '/.cache/vis-bak')
