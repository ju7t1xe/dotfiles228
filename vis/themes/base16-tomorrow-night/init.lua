-- base16-vis (https://github.com/pshevtsov/base16-vis)
-- by Petr Shevtsov
-- Tomorrow Night scheme by Chris Kempson (http://chriskempson.com)

local lexers = vis.lexers

local colors = {
    ['dark0' ] = '#121212',
    ['dark1' ] = '#282a2e',
    ['dark2' ] = '#373b41',
    ['gray0' ] = '#909090',
    ['gray1' ] = '#b4b7b4',
    ['gray2' ] = '#c5c8c6',
    ['gray3' ] = '#cccccc',
    ['white' ] = '#e0e0e0',
    ['red'   ] = '#cc6666',
    ['orange'] = '#de935f',
    ['yellow'] = '#f0c674',
    ['teal'  ] = '#8abeb7',
    ['blue'  ] = '#81a2be',
    ['purple'] = '#b294bb',
}

lexers.colors = colors

local fg = ',fore:'..colors.gray2..','
local bg = ',back:'..colors.dark0..','

lexers.STYLE_DEFAULT = bg..fg
lexers.STYLE_NOTHING = bg
lexers.STYLE_CLASS = 'fore:'..colors.blue
lexers.STYLE_COMMENT = 'fore:'..colors.gray0..',italics'
lexers.STYLE_CONSTANT = 'fore:'..colors.purple
lexers.STYLE_DEFINITION = 'fore:'..colors.red
lexers.STYLE_ERROR = 'fore:'..colors.red..',italics'
lexers.STYLE_FUNCTION = 'fore:'..colors.purple
lexers.STYLE_KEYWORD = 'fore:'..colors.teal..',bold'
lexers.STYLE_LABEL = 'fore:'..colors.blue
lexers.STYLE_NUMBER = 'fore:'..colors.purple
lexers.STYLE_OPERATOR = 'fore:'..colors.gray2
lexers.STYLE_REGEX = 'fore:'..colors.teal
lexers.STYLE_STRING = 'fore:'..colors.yellow
lexers.STYLE_PREPROCESSOR = 'fore:'..colors.blue
lexers.STYLE_TAG = 'fore:'..colors.yellow
lexers.STYLE_TYPE = 'fore:'..colors.blue..',bold'
lexers.STYLE_VARIABLE = 'fore:'..colors.blue
lexers.STYLE_WHITESPACE = 'fore:'..colors.dark2
lexers.STYLE_EMBEDDED = 'fore:'..colors.orange
lexers.STYLE_IDENTIFIER = 'fore:'..colors.gray3

lexers.STYLE_LINENUMBER = 'fore:'..colors.dark2..',back:'..colors.dark0
lexers.STYLE_CURSOR = 'fore:'..colors.dark0..',back:'..colors.gray2
lexers.STYLE_CURSOR_PRIMARY = 'fore:'..colors.dark0..',back:'..colors.gray3
lexers.STYLE_CURSOR_LINE = 'back:'..colors.dark1
lexers.STYLE_COLOR_COLUMN = 'back:'..colors.dark1
lexers.STYLE_SELECTION = 'fore:'..colors.yellow..',back:'..colors.dark2
lexers.STYLE_STATUS = 'fore:'..colors.gray0..',back:'..colors.dark1
lexers.STYLE_STATUS_FOCUSED = 'fore:'..colors.white..',back:'..colors.dark1
lexers.STYLE_SEPARATOR = lexers.STYLE_DEFAULT
lexers.STYLE_INFO = 'fore:default,back:default,bold'
lexers.STYLE_EOF = ''
