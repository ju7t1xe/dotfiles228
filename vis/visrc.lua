-- load standard vis module, providing parts of the Lua API
require('vis')

vis:map(vis.modes.INSERT, '<C-d>', '<vis-delete-char-next>')
vis:map(vis.modes.NORMAL, 'X', 'Vx')
vis:map(vis.modes.NORMAL, 'x', '"_<vis-delete-char-next>')
vis:map(vis.modes.VISUAL, '=', ':| clang-format --style="{BasedOnStyle: webkit, IndentWidth: 4, UseTab: Never, AlwaysBreakAfterReturnType: TopLevelDefinitions, AlignAfterOpenBracket: Align, AlignArrayOfStructures: Left, IndentCaseLabels: true, IndentPPDirectives: AfterHash, PackConstructorInitializers: NextLine}"<Enter>')
vis:map(vis.modes.NORMAL, ' a', '$i <Escape>p')

vis:map(vis.modes.VISUAL, '<C-c>', ':> wl-copy 2>/dev/null<Enter><Escape>')
vis:map(vis.modes.NORMAL, '<C-c>', 'V:> wl-copy 2>/dev/null<Enter><Escape>^')
vis:map(vis.modes.NORMAL, '<C-v>', '"+p')
vis:map(vis.modes.VISUAL, '<C-v>', '"+pa<Escape>')
vis:map(vis.modes.INSERT, '<C-v>', '<Escape>"+pa')
vis:map(vis.modes.NORMAL, '_', '<Home>')

vis.events.subscribe(vis.events.INIT, function()
	-- Your global configuration options
	vis:command('set theme base16-tomorrow-night')
	vis:command('set autoindent on')
	vis:command('set shell "/bin/sh"')
	vis:command('set ignorecase on')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win) -- luacheck: no unused args
	-- Your per window configuration options e.g.
	vis:command('set rnu on')
	-- vis:command('set show-tabs on')
	vis:command('set expandtab on')
	vis:command('set tabwidth 4')
end)

require('plugins')
require('title')
require('filetype')
require('comments')
